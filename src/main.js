import Vue from 'vue'
import App from './App.vue'
import VueRouter from 'vue-router'
import HomePage from "@/pages/HomePage";

Vue.use(VueRouter)
Vue.config.productionTip = false

const routes = [
    {path: '/', component: HomePage},
    {path: '/login', component: null}
]

new Vue({
    render: h => h(App),
    router: new VueRouter({routes})
}).$mount('#app')
